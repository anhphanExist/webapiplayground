﻿using Microsoft.Extensions.DependencyInjection;
using NLog;

namespace WebapiPlayGround.StartupConfig
{
    public static class LoggerConfiguration
    {
        public static void ConfigureLoggerService(this IServiceCollection services) =>
            services.AddScoped<ILoggerManager, LoggerManager>();
    }
    
    public interface ILoggerManager
    {
        void LogInfo(string message);
        void LogWarn(string message);
        void LogDebug(string message);
        void LogError(string message);
    }
    
    public class LoggerManager : ILoggerManager
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();

        public LoggerManager()
        {
            
        }
        public void LogDebug(string message)
        {
            Logger.Debug(message);
        }
        public void LogError(string message)
        {
            Logger.Error(message);
        }
        public void LogInfo(string message)
        {
            Logger.Info(message);
        }
        public void LogWarn(string message)
        {
            Logger.Warn(message);
        }
    }
}