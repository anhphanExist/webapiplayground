﻿using Microsoft.Extensions.DependencyInjection;

namespace WebapiPlayGround.StartupConfig
{
    public static class CorsConfiguration
    {
        public const string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        
        public static void ConfigureCors(this IServiceCollection services) =>
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });
    }
}