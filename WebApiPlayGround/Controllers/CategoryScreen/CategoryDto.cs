﻿using WebApiPlayGround.Repositories.Filtering;

namespace WebApiPlayGround.Controllers.CategoryScreen
{
    public class CategoryDto
    {
        public string Name { get; set; }
        public CategoryType Type { get; set; }
    }
}