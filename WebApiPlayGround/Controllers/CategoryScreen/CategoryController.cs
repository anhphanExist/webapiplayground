﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Services;

namespace WebApiPlayGround.Controllers.CategoryScreen
{
    public class CategoryRoute : RootRoute
    {
        public const string Default = Base + "/category";
        public const string List = Default + "/list";
    }

    public class CategoryController : ApiController
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            this._categoryService = categoryService;
        }

        [Route(CategoryRoute.List), HttpPost]
        public async Task<List<CategoryDto>> List()
        {
            CategoryFilter filter = new CategoryFilter
            {
                OrderBy = CategoryOrder.Name
            };

            List<Category> category = await _categoryService.List(filter);
            List<CategoryDto> res = new List<CategoryDto>();
            category.ForEach(c =>
            {
                res.Add(new CategoryDto
                {
                    Type = c.Type,
                    Name = c.Name
                });
            });
            return res;
        }
    }
}