﻿namespace WebApiPlayGround.Controllers.WalletScreen
{
    public class WalletTransferRequestDto
    {
        public string SourceWalletName { get; set; }
        public string DestWalletName { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
    }
}