﻿using System;

namespace WebApiPlayGround.Controllers.WalletScreen
{
    public class WalletDto
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
    }
}
