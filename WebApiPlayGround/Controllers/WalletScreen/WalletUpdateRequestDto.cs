﻿using System;

namespace WebApiPlayGround.Controllers.WalletScreen
{
    public class WalletUpdateRequestDto
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public string NewName { get; set; }
    }
}