﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiPlayGround.Repositories;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Controllers.WalletScreen
{
    public class WalletRoute : RootRoute
    {
        public const string Default = Base + "/wallet";
        public const string List = Default + "/list";
        public const string Create = Default + "/create";
        public const string Update = Default + "/update";
        public const string Delete = Default + "/delete";
        public const string Transfer = Default + "/transfer";
    }

    [Route("api/{controller}")]
    public class WalletController : ApiController
    {
        private readonly IWalletService _walletService;
        public WalletController(IWalletService walletService)
        {
            this._walletService = walletService;
        }

        [AllowAnonymous]
        [Route(WalletRoute.List), HttpPost]
        public async Task<List<WalletDto>> List()
        {
            WalletFilter walletFilter = new WalletFilter()
            {
                Balance = new DecimalFilter() { GreaterEqual = 1000 },
                Name = new StringFilter() { Contains = "User" },
                Selects = WalletSelect.All
            };
            List<Wallet> wallets = await _walletService.List(walletFilter);
            List<WalletDto> walletDtOs = new List<WalletDto>();
            wallets.ForEach(w => walletDtOs.Add(new WalletDto
            {
                UserId = w.UserId,
                Name = w.Name,
                Balance = w.Balance
            }));
            return walletDtOs;
        }

        [Route(WalletRoute.Create), HttpPost]
        public async Task<WalletDto> Create([FromBody] WalletDto walletRequestDto)
        {
            Wallet requestWallet = new Wallet
            {
                UserId = CurrentUserId,
                Balance = walletRequestDto.Balance,
                Name = walletRequestDto.Name
            };
            Wallet resultWallet = await _walletService.Create(requestWallet);
            return new WalletDto
            {
                Name = resultWallet.Name,
                Balance = resultWallet.Balance,
                UserId = resultWallet.UserId,
            };

        }

        [Route(WalletRoute.Update), HttpPost]
        public async Task<WalletDto> Update([FromBody] WalletUpdateRequestDto walletUpdateRequestDto) 
        {
            Wallet requestWallet = new Wallet
            {
                UserId = CurrentUserId,
                Balance = walletUpdateRequestDto.Balance,
                Name = walletUpdateRequestDto.Name
            };
            Wallet resultWallet = await _walletService.UpdateWalletName(requestWallet, walletUpdateRequestDto.NewName);
            return new WalletDto
            {
                Name = resultWallet.Name,
                Balance = resultWallet.Balance,
                UserId = resultWallet.UserId,
            };
        }

        [Route(WalletRoute.Delete), HttpPost]
        public async Task<WalletDto> Delete([FromBody] WalletDto walletRequestDto) 
        {
            Wallet requestWallet = new Wallet
            {
                UserId = CurrentUserId,
                Name = walletRequestDto.Name
            };
            Wallet resultWallet = await _walletService.Delete(requestWallet);
            return new WalletDto
            {
                Name = resultWallet.Name,
                Balance = resultWallet.Balance,
                UserId = resultWallet.UserId
            };
        }

        [Route(WalletRoute.Transfer), HttpPost]
        public async Task<WalletTransferResponseDto> Transfer([FromBody] WalletTransferRequestDto walletTransferRequestDto)
        {
            // Tao 2 wallet BO de gui cho Service xu ly Transfer
            Wallet sourceWallet = new Wallet
            {
                Name = walletTransferRequestDto.SourceWalletName,
                UserId = CurrentUserId
            };
            Wallet destWallet = new Wallet
            {
                Name = walletTransferRequestDto.DestWalletName,
                UserId = CurrentUserId
            };
            Tuple<Wallet, Wallet> res = await _walletService.Transfer(
                Tuple.Create(sourceWallet, destWallet), 
                walletTransferRequestDto.Amount, 
                walletTransferRequestDto.Note);

            // Tao response object
            WalletTransferResponseDto resDto = new WalletTransferResponseDto
            {
                SourceWalletName = res.Item1.Name,
                DestWalletName = res.Item2.Name,
                UserId = res.Item1.UserId
            };
            return resDto;
        }
    }
}