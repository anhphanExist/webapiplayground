﻿using System;

namespace WebApiPlayGround.Controllers.WalletScreen
{
    public class WalletTransferResponseDto
    {
        public string UserId { get; set; }
        public string SourceWalletName { get; set; }
        public string DestWalletName { get; set; }
    }
}
