﻿namespace WebApiPlayGround.Controllers.TransactionScreen
{
    public class TransactionMonthGroupRequestDto
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string WalletName { get; set; }
    }
}