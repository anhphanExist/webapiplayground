﻿using System;

namespace WebApiPlayGround.Controllers.TransactionScreen
{
    public class TransactionDto
    {
        public string WalletName { get; set; }
        public string CategoryName { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public DateTime Date { get; set; }
    }
}