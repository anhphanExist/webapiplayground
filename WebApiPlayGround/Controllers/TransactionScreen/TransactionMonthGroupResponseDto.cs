﻿using System.Collections.Generic;

namespace WebApiPlayGround.Controllers.TransactionScreen
{
    public class TransactionMonthGroupResponseDto
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal Income { get; set; }
        public decimal Expense { get; set; }
        public decimal NetIncome { get; set; }
        public List<TransactionDayGroupDto> TransactionDayGroups { get; set; }
    }
}