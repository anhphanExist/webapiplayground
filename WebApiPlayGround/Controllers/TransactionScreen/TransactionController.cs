﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiPlayGround.Repositories;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;

namespace WebApiPlayGround.Controllers.TransactionScreen
{
    public class TransactionRoute : RootRoute
    {
        public const string Default = Base + "/transaction";
        public const string Create = Default + "/create";
        public const string GetTransactionMonthGroup = Default + "/get-transaction-month-group";
    }

    public class TransactionController : ApiController
    {
        private readonly ITransactionService _transactionService;
        public TransactionController(ITransactionService transactionService)
        {
            this._transactionService = transactionService;
        }

        [Route(TransactionRoute.Create), HttpPost]
        public async Task<TransactionDto> Create([FromBody] TransactionDto transactionRequestDto)
        {
            Transaction newTransaction = new Transaction {
                WalletName = transactionRequestDto.WalletName,
                Amount = transactionRequestDto.Amount,
                CategoryName = transactionRequestDto.CategoryName,
                Date = transactionRequestDto.Date,
                Note = transactionRequestDto.Note,
                UserId = CurrentUserId
            };

            Transaction res = await _transactionService.Create(newTransaction);
            return new TransactionDto
            {
                WalletName = res.WalletName,
                Note = res.Note,
                Date = res.Date,
                CategoryName = res.CategoryName,
                Amount = res.Amount
            };
        }

        [Route(TransactionRoute.GetTransactionMonthGroup), HttpPost]
        public async Task<TransactionMonthGroupResponseDto> GetTransactionMonthGroup([FromBody] TransactionMonthGroupRequestDto transactionMonthGroupRequestDTO)
        {
            // Tao filter cho MonthGroup
            TransactionMonthGroupFilter filter = new TransactionMonthGroupFilter
            {
                Month = transactionMonthGroupRequestDTO.Month,
                Year = transactionMonthGroupRequestDTO.Year,
                WalletName = transactionMonthGroupRequestDTO.WalletName,
                UserId = CurrentUserId
            };

            // Get MonthGroup tu filter
            TransactionMonthGroup res = await _transactionService.GetTransactionMonthGroup(filter);
            
            // Tao List DayGroupDTO de gan vao trong ket qua
            List<TransactionDayGroupDto> transactionDayGroupDtos = new List<TransactionDayGroupDto>();

            // Voi moi DayGroupDTO thi gan list transactionDTO
            res.TransactionDayGroups.ToList().ForEach(tdg => 
            {
                // Tao list transactionDTO
                List<TransactionDto> transactionDtos = new List<TransactionDto>();
                tdg.Transactions.ToList().ForEach(t => transactionDtos.Add(new TransactionDto
                {
                    WalletName = t.WalletName,
                    Date = t.Date,
                    Amount = t.Amount,
                    CategoryName = t.CategoryName,
                    Note = t.Note
                }));

                // Gan list transactionDTO vao DayGroupDTO
                transactionDayGroupDtos.Add(new TransactionDayGroupDto
                {
                    Date = tdg.Date,
                    Inflow = tdg.Income,
                    Outflow = tdg.Expense,
                    Transactions = transactionDtos
                });
            });
            
            // Tra ket qua MonthGroupDTO
            return new TransactionMonthGroupResponseDto
            {
                TransactionDayGroups = transactionDayGroupDtos,
                NetIncome = res.NetIncome,
                Income = res.Income,
                Expense = res.Expense,
                Month = res.Month,
                Year = res.Year
            };
        }
    }
}