﻿using System;
using System.Collections.Generic;

namespace WebApiPlayGround.Controllers.TransactionScreen
{
    public class TransactionDayGroupDto
    {
        public DateTime Date { get; set; }
        public decimal Inflow { get; set; }
        public decimal Outflow { get; set; }
        public List<TransactionDto> Transactions { get; set; }
    }
}