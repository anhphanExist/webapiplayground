﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiPlayGround.Services.DatabaseSeeding;

namespace WebApiPlayGround.Controllers
{
    public class SeedDataRoute : RootRoute
    {
        public const string Default = Base + "/seed-data";
        public const string Init = Default + "/init";
    }

    [AllowAnonymous]
    public class SeedDataController : ControllerBase
    {
        private readonly IDataInitiationService _dataInitiationService;

        public SeedDataController(IDataInitiationService dataInitiationService)
        {
            _dataInitiationService = dataInitiationService;
        }

        [HttpPost]
        [Route(SeedDataRoute.Init)]
        public async Task<bool> Init()
        {
            return await _dataInitiationService.Init();
        }
        
    }
}