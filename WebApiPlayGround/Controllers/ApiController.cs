﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApiPlayGround.Controllers
{
    public class RootRoute
    {
        public const string Base = "api/Moneyger";
    }
    
    [Authorize]
    public class ApiController : ControllerBase
    {
        public string CurrentUserId
        {
            get
            {
                return User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
            }
        }
    }
}