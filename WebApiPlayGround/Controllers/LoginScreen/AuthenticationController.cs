﻿using Microsoft.AspNetCore.Mvc;
using WebapiPlayGround.StartupConfig;

namespace WebApiPlayGround.Controllers.LoginScreen
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILoggerManager _logger;
        public AuthenticationController (ILoggerManager logger)
        {
            _logger = logger;
        }
    }
}