﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories;

namespace WebApiPlayGround.Controllers.RegisterScreen
{
    [Route("api/Moneyger")]
    [AllowAnonymous]
    public class RegisterController : ControllerBase
    {
        private readonly IUserService _userService;
        public RegisterController(IUserService userService)
        {
            this._userService = userService;
        }

       [Route("signup"), HttpPost]
        public async Task<RegisterResponseDto> Signup([FromBody] RegisterRequestDto registerUserDto)
        {
            throw new NotImplementedException();
        }
    }
}