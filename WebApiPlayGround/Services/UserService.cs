﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Filtering;

namespace WebApiPlayGround.Repositories
{
    public interface IUserService
    {
        Task<User> Login(User user);
        Task<User> ChangePassword(User user, string newPassword);
        Task<User> Create(User user);
    }
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<User> Login(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<User> ChangePassword(User user, string newPassword)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    throw new NotImplementedException();
                }
                catch (Exception e)
                {
                    await _unitOfWork.Rollback();
                    return user;
                }
            }
        }

        public async Task<User> Get(UserFilter filter)
        {
            throw new NotImplementedException();
        }

        public async Task<User> Get(Guid Id)
        {
            return await _unitOfWork.UserRepository.Get(Id);
        }

        public async Task<User> Create(User user)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    throw new NotImplementedException();
                }
                catch (Exception e)
                {
                    await _unitOfWork.Rollback();
                    return user;
                }
            }
        }
    }
}