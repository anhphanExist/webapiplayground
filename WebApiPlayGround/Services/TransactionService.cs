﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;

namespace WebApiPlayGround.Repositories
{
    public interface ITransactionService
    {
        Task<Transaction> Get(Guid Id);
        Task<Transaction> Get(TransactionFilter filter);
        Task<Transaction> Create(Transaction transaction);
        Task<Transaction> Update(Transaction transaction);
        Task<Transaction> Delete(Transaction transaction);
        Task<int> Count(TransactionFilter filter);
        Task<List<Transaction>> List(TransactionFilter filter);
        Task<TransactionMonthGroup> GetTransactionMonthGroup(TransactionMonthGroupFilter filter);
    }
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransactionService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            
        }
        public async Task<int> Count(TransactionFilter filter)
        {
            if (filter == null) filter = new TransactionFilter { };
            return await _unitOfWork.TransactionRepository.Count(filter);
        }

        public async Task<Transaction> Create(Transaction transaction)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    CategoryFilter categoryFilter = new CategoryFilter
                    {
                        Name = new StringFilter { Equal = transaction.CategoryName }
                    };
                    Category category = await _unitOfWork.CategoryRepository.Get(categoryFilter);

                    WalletFilter walletFilter = new WalletFilter
                    {
                        Name = new StringFilter { Equal = transaction.WalletName },
                        UserId = new StringFilter() { Equal = transaction.UserId }
                    };
                    Wallet wallet = await _unitOfWork.WalletRepository.Get(walletFilter);


                    transaction.Id = Guid.NewGuid();
                    transaction.WalletId = wallet.Id;
                    transaction.CategoryId = category.Id;
                    if (category.Type == CategoryType.Income)
                        wallet.Balance += transaction.Amount;
                    else wallet.Balance -= transaction.Amount;

                    await _unitOfWork.WalletRepository.Update(wallet);
                    await _unitOfWork.TransactionRepository.Create(transaction);
                    await _unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();
                }
            }
            return transaction;
        }

        public async Task<Transaction> Delete(Transaction transaction)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    await _unitOfWork.TransactionRepository.Delete(transaction.Id);
                    await _unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();
                }
            }
            return transaction;
        }

        public async Task<Transaction> Get(Guid Id)
        {
            return await _unitOfWork.TransactionRepository.Get(Id);
        }

        public async Task<Transaction> Get(TransactionFilter filter)
        {
            return await _unitOfWork.TransactionRepository.Get(filter);
        }

        public async Task<List<Transaction>> List(TransactionFilter filter)
        {
            if (filter == null) filter = new TransactionFilter { };
            return await _unitOfWork.TransactionRepository.List(filter);
        }

        public async Task<Transaction> Update(Transaction transaction)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    await _unitOfWork.TransactionRepository.Update(transaction);
                    await _unitOfWork.Commit();
                    return await this._unitOfWork.TransactionRepository.Get(transaction.Id);
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();
                    return transaction;
                }
            }
        }

        public async Task<TransactionMonthGroup> GetTransactionMonthGroup(TransactionMonthGroupFilter filter)
        {
            // Filter theo thang va nam de list transaction day group theo thang do
            TransactionDayGroupFilter transactionDayGroupFilter = new TransactionDayGroupFilter
            {
                Month = filter.Month,
                Year = filter.Year,
                WalletName = filter.WalletName,
                UserId = filter.UserId
            };
            List<TransactionDayGroup> transactionDayGroups = await ListTransactionDayGroup(transactionDayGroupFilter);

            // Tinh toan voi outflow < 0 va inflow > 0
            decimal inflow = 0;
            decimal outflow = 0;
            transactionDayGroups.ForEach(tdg =>
            {
                inflow += tdg.Income;
                outflow += tdg.Expense;
            });

            // Tra ve transaction month group
            return new TransactionMonthGroup
            {
               Expense = outflow,
               Income = inflow,
               NetIncome = inflow + outflow,
               Month = filter.Month,
               Year = filter.Year,
               TransactionDayGroups = transactionDayGroups
            };
        }

        private async Task<List<TransactionDayGroup>> ListTransactionDayGroup(TransactionDayGroupFilter filter)
        {
            // Khoi tao List chua ket qua
            List<TransactionDayGroup> result = new List<TransactionDayGroup>();

            // Lay het ngay trong thang filter ra
            List<DateTime> dates = GetDates(filter.Year, filter.Month);

            // Trong moi ngay ma co Transaction thi tao 1 TransactionDayGroup
            foreach (DateTime date in dates)
            {
                // Tao filter theo tung ngay trong thang
                TransactionFilter transactionFilter = new TransactionFilter
                {
                    Date = new DateTimeFilter { Equal = date },
                    WalletName = new StringFilter { Equal = filter.WalletName },
                    UserId = new StringFilter() { Equal = filter.UserId }
                };

                // List transactions trong ngay hom do
                List<Transaction> transactions = await List(transactionFilter);

                // Neu ngay hom do co transactions thi tao 1 TransactionDayGroup moi
                if (transactions != null)
                {
                    if (transactions.Count > 0)
                    {
                        decimal inflow = 0;
                        decimal outflow = 0;
                        transactions.ForEach(t =>
                        {
                            if (t.Amount < 0) outflow += t.Amount;
                            else inflow += t.Amount;
                        });
                        result.Add(new TransactionDayGroup
                        {
                            Date = date,
                            Transactions = transactions,
                            Income = inflow,
                            Expense = outflow
                        });
                    }
                }

            }

            // Tra ket qua
            return result;
            
        }

        private static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))  // Days: 1, 2 ... 31 etc.
                             .Select(day => new DateTime(year, month, day)) // Map each day to a date
                             .ToList(); // Load dates into a list
        }
    }
}