﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;
using WebapiPlayGround.Services.DatabaseSeeding;

namespace WebApiPlayGround.Services.DatabaseSeeding
{
    public interface IDataInitiationService
    {
        Task<bool> Init();
    }
    
    public class DataInitiationService : IDataInitiationService
    {
        private readonly RepositoryContext _repositoryContext;
        private readonly DataInit _dataInit;

        public DataInitiationService(RepositoryContext repositoryContext, UserManager<UserDao> userManager)
        {
            _repositoryContext = repositoryContext;
            _dataInit = new DataInit(repositoryContext, userManager);
        }

        public async Task<bool> Init()
        {
            if (await IsInitializedOnce())
                return false;
            return await _dataInit.Init();
        }
        
        private async Task<bool> IsInitializedOnce()
        {
            return await _repositoryContext.Wallets.AsNoTracking().AnyAsync();
        }
    }
}