﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using WebApiPlayGround.Helpers;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebapiPlayGround.Services.DatabaseSeeding
{
    public class UserInit : CommonInit
    {
        private readonly UserManager<UserDao> _userManager;
        public List<string> UserCodes { get; private set; }
        public UserInit(RepositoryContext repositoryContext, UserManager<UserDao> userManager) : base(repositoryContext)
        {
            _userManager = userManager;
            UserCodes = new List<string>();
        }

        public async Task<List<string>> Init(int count = 1)
        {
            List<string> returnList = new List<string>();
            string baseCode = "User";

            for (int i = 0; i < count; i++)
            {
                string code = baseCode + i.ToString();
                var user = new UserDao()
                {
                    Id = GuidHelpers.CreateGuid(code).ToString(),
                    UserName = code,
                    Email = $"{code}@gmail.com",
                    EmailConfirmed = true,
                    FirstName = code,
                    LastName = code,
                    PhoneNumber = "038 943 3443"
                };

                var chkUser = await _userManager.CreateAsync(user, "Asdfgh1@3");
                if (!chkUser.Succeeded)
                    throw new Exception($"Create user {user.UserName} fail ");
                await _userManager.AddToRoleAsync(user, "Administrator");
                returnList.Add(code);
            }

            UserCodes.AddRange(returnList);
            return returnList;
        }
    }
}
