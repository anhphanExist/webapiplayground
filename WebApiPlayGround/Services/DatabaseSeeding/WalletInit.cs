﻿using System.Collections.Generic;
using WebApiPlayGround.Helpers;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebapiPlayGround.Services.DatabaseSeeding
{
    public class WalletInit : CommonInit
    {
        public List<string> WalletCodes { get; private set; }
        public WalletInit(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            WalletCodes = new List<string>();
        }

        public List<string> Init(string userId, int count = 1)
        {
            List<string> returnList = new List<string>();
            string baseCode = "Wallet";

            for (int i = 0; i < count; i++)
            {
                string code = userId + "." + baseCode + i.ToString();
                
                RepositoryContext.Wallets.Add(new WalletDao
                {
                    Id = GuidHelpers.CreateGuid(code),
                    Balance = i * 1000000,
                    Name = code,
                    UserId = GuidHelpers.CreateGuid(userId).ToString()
                });
                returnList.Add(code);
            }

            WalletCodes.AddRange(returnList);
            return returnList;
        }
    }
}
