﻿using System;
using System.Security.Cryptography;
using System.Text;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebapiPlayGround.Services.DatabaseSeeding
{
    public class CommonInit
    {
        protected readonly RepositoryContext RepositoryContext;
        public CommonInit(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }
    }
}
