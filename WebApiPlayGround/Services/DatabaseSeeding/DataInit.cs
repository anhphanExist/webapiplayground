﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebapiPlayGround.Services.DatabaseSeeding
{
    public class DataInit
    {
        private readonly RepositoryContext _repositoryContext;
        private readonly UserInit _userInit;
        private readonly WalletInit _walletInit;
        private readonly CategoryInit _categoryInit;
        private readonly TransactionInit _transactionInit;

        public DataInit(RepositoryContext repositoryContext, UserManager<UserDao> userManager)
        {
            _repositoryContext = repositoryContext;
            _userInit = new UserInit(repositoryContext, userManager);
            _walletInit = new WalletInit(repositoryContext);
            _categoryInit = new CategoryInit(repositoryContext);
            _transactionInit = new TransactionInit(repositoryContext);
        }

        public async Task<bool> Init()
        {
            Clean();
            using (await _repositoryContext.Database.BeginTransactionAsync())
            {
                try
                {
                    await InitUser();
                    InitCategory();
                    InitWallet();
                    InitTransaction();
                    await _repositoryContext.SaveChangesAsync();
                    await _repositoryContext.Database.CommitTransactionAsync();
                    return true;
                }
                catch (Exception)
                {
                    await _repositoryContext.Database.RollbackTransactionAsync();
                    throw;
                }
            }
        }

        private void InitTransaction()
        {
            for (int i = 0; i < _walletInit.WalletCodes.Count; i++)
            {
                for (int j = 0; j < _categoryInit.CategoryCodes.Count; j++)
                {
                    _transactionInit.Init(_walletInit.WalletCodes[i], _categoryInit.CategoryCodes[j], 20);
                }
            }
            
        }

        private void InitWallet()
        {
            for (int i = 0; i < _userInit.UserCodes.Count; i++)
            {
                _walletInit.Init(_userInit.UserCodes[i], 10);
            }
        }

        private void InitCategory()
        {
            _categoryInit.Init(10);
        }

        private async Task InitUser()
        {
            await _userInit.Init(10);
        }

        public void Clean()
        { 
            
        }
    }
}
