﻿using System.Collections.Generic;
using WebApiPlayGround.Helpers;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebapiPlayGround.Services.DatabaseSeeding
{
    public class CategoryInit : CommonInit
    {
        public List<string> CategoryCodes { get ; private set; }
        public CategoryInit(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            CategoryCodes = new List<string>();
        }

        public List<string> Init(int count = 1)
        {
            List<string> returnList = new List<string>();
            List<string> outflowCategories = new List<string>
            {
                "Bills",
                "Business",
                "Education",
                "Entertainment",
                "Family",
                "Punishment Charge",
                "Food",
                "Gifts and Donation",
                "Health and Fitness",
                "Insurances",
                "Investment",
                "Shopping",
                "OtherExpenses",
                "Transportation",
                "Travel",
                "Withdrawal"
            };
            List<string> inflowCategories = new List<string>
            {
                "Award",
                "Gifts",
                "Interest Money",
                "OtherIncome",
                "Salary",
                "Selling"
            };

            foreach (string item in outflowCategories)
            {
                RepositoryContext.Add(new CategoryDao
                {
                    Id = GuidHelpers.CreateGuid(item),
                    Type = false,
                    Name = item
                });
                returnList.Add(item);
            }
            
            foreach (string item in inflowCategories)
            {
                RepositoryContext.Add(new CategoryDao
                {
                    Id = GuidHelpers.CreateGuid(item),
                    Type = true,
                    Name = item
                });
                returnList.Add(item);
            }

            RepositoryContext.Categories.Add(new CategoryDao
            {
                Id = GuidHelpers.CreateGuid("Wallet Transfer Source"),
                Type = false,
                Name = "Wallet Transfer",
            });
            returnList.Add("Wallet Transfer Source");
            RepositoryContext.Categories.Add(new CategoryDao
            {
                Id = GuidHelpers.CreateGuid("Wallet Transfer Destination"),
                Type = true,
                Name = "Wallet Transfer"
            });
            returnList.Add("Wallet Transfer Destination");

            CategoryCodes.AddRange(returnList);
            return returnList;
        }
    }
}
