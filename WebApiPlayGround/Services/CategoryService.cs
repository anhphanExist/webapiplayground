﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Filtering;

namespace WebApiPlayGround.Services
{
    public interface ICategoryService
    {
        Task<Category> Get(Guid Id);
        Task<int> Count(CategoryFilter filter);
        Task<List<Category>> List(CategoryFilter filter);
    }

    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CategoryService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<int> Count(CategoryFilter filter)
        {
            int count = await _unitOfWork.CategoryRepository.Count(filter);
            return count;
        }

        public async Task<Category> Get(Guid Id)
        {
            if (Id == Guid.Empty) return null;
            Category categories = await _unitOfWork.CategoryRepository.Get(Id);
            if (categories == null) return null;
            return categories;
        }

        public async Task<List<Category>> List(CategoryFilter filter)
        {
            List<Category> list = await _unitOfWork.CategoryRepository.List(filter);
            return list;
        }
    }
}