﻿using Microsoft.Extensions.DependencyInjection;
using WebApiPlayGround.Repositories;
using WebApiPlayGround.Services.DatabaseSeeding;

namespace WebApiPlayGround.Services.Configuration
{
    public static class ServicesRegistration
    {
        public static void ConfigureServicesLayer(this IServiceCollection services, bool registerOdbc = false)
        {
            services.AddScoped<IDataInitiationService, DataInitiationService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ITransactionService, TransactionService>();
            
        }
    }
}