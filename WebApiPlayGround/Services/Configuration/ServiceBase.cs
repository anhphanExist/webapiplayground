﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApiPlayGround.Services.BusinessEntities.Base;
using WebApiPlayGround.Repositories.Filtering.Base;

namespace WebApiPlayGround.Repositories.Configuration
{
    public interface IServiceBase<TEntity, TDto, TFilter>
    {
        Task<int> Count(TFilter filter);
        Task<List<TEntity>> List(TFilter filter);
        Task<TEntity> Create(TDto dataTransferObject);
        Task<TEntity> Update(TDto dataTransferObject);
        Task<TEntity> Delete(TDto dataTransferObject);
    }
    
    public abstract class ServiceBase<TEntity, TDto, TFilter> : IServiceBase<TEntity, TDto, TFilter>
    {
        public async Task<int> Count(TFilter filter)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<TEntity>> List(TFilter filter)
        {
            throw new System.NotImplementedException();
        }

        public async Task<TEntity> Create(TDto dataTransferObject)
        {
            throw new System.NotImplementedException();
        }

        public async Task<TEntity> Update(TDto dataTransferObject)
        {
            throw new System.NotImplementedException();
        }

        public async Task<TEntity> Delete(TDto dataTransferObject)
        {
            throw new System.NotImplementedException();
        }
    }
}