﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApiPlayGround.Helpers;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;
using WebApiPlayGround.Repositories.Paging;

namespace WebApiPlayGround.Repositories
{
    public interface IWalletService
    {
        Task<Wallet> Get(Guid Id);
        Task<Wallet> Get(WalletFilter filter);
        Task<Wallet> Create(Wallet wallet);
        Task<Wallet> UpdateWalletName(Wallet wallet, string newName);
        Task<Wallet> Delete(Wallet wallet);
        Task<int> Count(WalletFilter filter);
        Task<List<Wallet>> List(WalletFilter filter);
        Task<Tuple<Wallet, Wallet>> Transfer(Tuple<Wallet, Wallet> transferWalletTuple, decimal transferAmount, string note);
    }
    public class WalletService : IWalletService
    {
        private readonly IUnitOfWork _unitOfWork;
        public WalletService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<int> Count(WalletFilter filter)
        {
            if (filter == null) filter = new WalletFilter { };
            return await _unitOfWork.WalletRepository.Count(filter);
        }

        public async Task<Wallet> Create(Wallet wallet)
        {
            wallet.Id = Guid.NewGuid();

            using (_unitOfWork.Begin())
            {
                try
                {
                    await _unitOfWork.WalletRepository.Create(wallet);
                    await _unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();
                }
            }
            return wallet;
        }

        public async Task<Wallet> Delete(Wallet wallet)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    WalletFilter walletFilter = new WalletFilter
                    {
                        UserId = new StringFilter() { Equal = wallet.UserId },
                        Name = new StringFilter { Equal = wallet.Name }
                    };
                    Wallet walletToDelete = await _unitOfWork.WalletRepository.Get(walletFilter);
                    await _unitOfWork.WalletRepository.Delete(walletToDelete.Id);
                    await _unitOfWork.Commit();
                    return walletToDelete;
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();
                    return wallet;
                }
            }
        }

        public async Task<Wallet> Get(WalletFilter filter)
        {
            return await _unitOfWork.WalletRepository.Get(filter);
        }
        public async Task<Wallet> Get(Guid Id)
        {
            return await _unitOfWork.WalletRepository.Get(Id);
        }

        public async Task<List<Wallet>> List(WalletFilter filter)
        {
            if (filter == null) 
                filter = new WalletFilter();
            return await _unitOfWork.WalletRepository.List(filter);
        }

        public async Task<Tuple<Wallet, Wallet>> Transfer(Tuple<Wallet, Wallet> transferWalletTuple, decimal transferAmount, string note)
        {
            using (_unitOfWork.Begin())
            {
                try
                {
                    // tao filter de tim 2 wallet yeu cau trong Repo
                    WalletFilter sourceWalletFilter = new WalletFilter
                    {
                        UserId = new StringFilter() { Equal = transferWalletTuple.Item1.UserId },
                        Name = new StringFilter { Equal = transferWalletTuple.Item1.Name }
                    };
                    WalletFilter destinationWalletFilter = new WalletFilter
                    {
                        UserId = new StringFilter() { Equal = transferWalletTuple.Item2.UserId },
                        Name = new StringFilter { Equal = transferWalletTuple.Item2.Name }
                    };

                    // Lay du lieu 2 wallet tu DB, tru tien cua sourceWallet va cong tien vao destWallet
                    Wallet source = await _unitOfWork.WalletRepository.Get(sourceWalletFilter);
                    Wallet dest = await _unitOfWork.WalletRepository.Get(destinationWalletFilter);
                    source.Balance -= transferAmount;
                    dest.Balance += transferAmount;

                    // Tao transaction o ca 2 wallet
                    Transaction sourceTransaction = new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = transferAmount,
                        Date = DateTime.Now,
                        Note = note,
                        WalletId = source.Id,
                        CategoryId = GuidHelpers.CreateGuid("Wallet Transfer Source")
                    };
                    Transaction destTransaction = new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = transferAmount,
                        Date = DateTime.Now,
                        Note = note,
                        WalletId = dest.Id,
                        CategoryId = GuidHelpers.CreateGuid("Wallet Transfer Destination")
                    };

                    // Update vao Repo
                    await _unitOfWork.WalletRepository.Update(source);
                    await _unitOfWork.TransactionRepository.Create(sourceTransaction);
                    await _unitOfWork.WalletRepository.Update(dest);
                    await _unitOfWork.TransactionRepository.Create(destTransaction);
                    await _unitOfWork.Commit();

                    // Tra ve tuple da transfer
                    return Tuple.Create(source, dest);
                }
                catch (Exception e)
                {
                    await _unitOfWork.Rollback();
                    return transferWalletTuple;
                }
            }
        }

        public async Task<Wallet> UpdateWalletName(Wallet wallet, string newName)
        {
            if (wallet == null)
                return null;

            using (_unitOfWork.Begin())
            {
                try
                {
                    WalletFilter filter = new WalletFilter
                    {
                        UserId = new StringFilter() { Equal = wallet.UserId },
                        Name = new StringFilter { Equal = wallet.Name }
                    };
                    wallet = await Get(filter);
                    wallet.Name = newName;

                    await _unitOfWork.WalletRepository.Update(wallet);
                    await _unitOfWork.Commit();
                    return await Get(new WalletFilter
                    {
                        Id = new GuidFilter { Equal = wallet.Id },
                        UserId = new StringFilter() { Equal = wallet.UserId },
                        Name = new StringFilter { Equal = wallet.Name },
                        Balance = new DecimalFilter { Equal = wallet.Balance }
                    });
                }
                catch (Exception ex)
                {
                    await _unitOfWork.Rollback();
                }
            }
            return wallet;
        }
    }
}