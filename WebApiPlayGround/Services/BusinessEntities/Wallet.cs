﻿using System;
using WebApiPlayGround.Services.BusinessEntities.Base;

namespace WebApiPlayGround.Services.BusinessEntities
{
    public class Wallet : BaseBusinessEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public string UserId { get; set; }
    }
}