﻿using System;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Services.BusinessEntities.Base;

namespace WebApiPlayGround.Services.BusinessEntities
{
    public class Category : BaseBusinessEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryType Type { get; set; }
        public string ImageUrl { get; set; }
    }
}