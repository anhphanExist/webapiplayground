﻿using System;
using System.Collections.Generic;
using WebApiPlayGround.Services.BusinessEntities.Base;
using WebApiPlayGround.Repositories.Filtering.Base;

namespace WebApiPlayGround.Services.BusinessEntities
{
    public class TransactionMonthGroup : BaseBusinessEntity
    {
        public TransactionMonthGroup()
        {
            TransactionDayGroups = new List<TransactionDayGroup>();
        }
        public decimal Income { get; set; }
        public decimal Expense { get; set; }
        public decimal NetIncome { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public List<TransactionDayGroup> TransactionDayGroups { get; set; }
    }
}