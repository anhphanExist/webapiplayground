﻿using System;
using System.Collections.Generic;
using WebApiPlayGround.Services.BusinessEntities.Base;

namespace WebApiPlayGround.Services.BusinessEntities
{
    public class TransactionDayGroup : BaseBusinessEntity
    {
        public TransactionDayGroup()
        {
            Transactions = new List<Transaction>();
        }
        public DateTime Date { get; set; } 
        public decimal Income { get; set; }
        public decimal Expense { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}