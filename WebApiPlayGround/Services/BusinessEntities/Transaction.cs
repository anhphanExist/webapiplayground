﻿using System;
using WebApiPlayGround.Services.BusinessEntities.Base;

namespace WebApiPlayGround.Services.BusinessEntities
{
    public class Transaction : BaseBusinessEntity
    {
        public Guid Id { get; set; }
        public Guid WalletId { get; set; }
        public string UserId { get; set; }
        public string WalletName { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public DateTime Date { get; set; }
    }
}