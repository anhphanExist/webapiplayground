﻿using System;
using WebApiPlayGround.Services.BusinessEntities.Base;

namespace WebApiPlayGround.Services.BusinessEntities
{
    public class User : BaseBusinessEntity
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}