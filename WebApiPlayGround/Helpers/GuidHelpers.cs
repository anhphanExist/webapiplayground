﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WebApiPlayGround.Helpers
{
    public static class GuidHelpers
    {
        public static Guid CreateGuid(string name)
        {
            MD5 md5 = MD5.Create();
            Byte[] myStringBytes = Encoding.Default.GetBytes(name);
            Byte[] hash = md5.ComputeHash(myStringBytes);
            return new Guid(hash);
        }
    }
}