﻿using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories.Filtering
{
    public class TransactionFilter : BaseFilter
    {
        public GuidFilter Id { get; set; }
        public StringFilter UserId { get; set; }
        public GuidFilter WalletId { get; set; }
        public StringFilter WalletName { get; set; }
        public GuidFilter CategoryId { get; set; }
        public StringFilter CategoryName { get; set; }
        public DateTimeFilter Date { get; set; }
        public TransactionOrder OrderBy { get; set; }
        public TransactionSelect Selects { get; set; }
        public TransactionFilter() : base()
        {

        }
    }
    public enum TransactionOrder
    {
        Cx,
        WalletId,
        CategoryId
    }
}