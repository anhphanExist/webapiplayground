﻿using System;
using WebApiPlayGround.Repositories.Filtering.Base;

namespace WebApiPlayGround.Repositories.Filtering
{
    public class TransactionMonthGroupFilter : BaseFilter
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string WalletName { get; set; }
        public string UserId { get; set; }
    }
}