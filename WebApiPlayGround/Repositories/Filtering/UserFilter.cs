﻿using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories.Filtering
{
    public class UserFilter : BaseFilter
    {
        public StringFilter UserName { get; set; }
        public StringFilter FirstName { get; set; }
        public StringFilter LastName { get; set; }
        public UserOrder OrderBy { get; set; }
        public UserSelect Selects { get; set; }

        public UserFilter() : base()
        {
            
        }
    }

    public enum UserOrder
    {
        Cx,
        Username,
        FirstName,
        LastName
    }
}