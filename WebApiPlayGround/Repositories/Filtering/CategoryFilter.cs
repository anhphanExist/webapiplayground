﻿using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories.Filtering
{
    public class CategoryFilter : BaseFilter
    {
        public GuidFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public bool? Type { get; set; }
        public CategoryOrder OrderBy { get; set; }
        public CategorySelect Selects { get; set; }
        public CategoryFilter() : base()
        {

        }
    }
    public enum CategoryOrder
    {
        Cx,
        Name,
        Type
    }

    public enum CategoryType
    {
        Expense = 0,
        Income = 1
    }
}