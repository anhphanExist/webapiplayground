﻿using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Filtering.Base.FilterType;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories.Filtering
{
    public class WalletFilter : BaseFilter
    {
        public GuidFilter Id { get; set; }
        public StringFilter Name { get; set; }
        public DecimalFilter Balance { get; set; }
        public StringFilter UserId { get; set; }
        public WalletOrder OrderBy { get; set; }
        public WalletSelect Selects { get; set; }
        public WalletFilter() : base()
        {

        }

    }
    public enum WalletOrder
    {
        Cx,
        Name,
        Balance
    }
}