﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebApiPlayGround.Repositories.Filtering.Base
{
    public class BaseFilter
    {
        private const int MaxPageSize = 500;
        private int _currentPage;
        private int _pageSize;

        public int CurrentPage
        {
            get => _currentPage;
            set => _currentPage = value == default ? 1 : value;
        }

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value == default || value > MaxPageSize) ? MaxPageSize : value;
        }

        public string SearchString { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OrderType OrderType { get; set; }

        public BaseFilter()
        {
            CurrentPage = 1;
            PageSize = MaxPageSize;
            OrderType = OrderType.ASC;
        }
    }

    public enum OrderType
    {
        ASC,
        DESC
    }
}