﻿namespace WebApiPlayGround.Repositories.Filtering.Base.FilterType
{
    public class DoubleFilter
    {
        public double? Equal { get; set; }
        public double? NotEqual { get; set; }
        public double? Less { get; set; }
        public double? LessEqual { get; set; }
        public double? Greater { get; set; }
        public double? GreaterEqual { get; set; }
    }
}