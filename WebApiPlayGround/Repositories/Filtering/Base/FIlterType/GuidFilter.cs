﻿using System;

namespace WebApiPlayGround.Repositories.Filtering.Base.FilterType
{
    public class GuidFilter
    {
        public Guid? Equal { get; set; }
        public Guid? NotEqual { get; set; }
    }
}