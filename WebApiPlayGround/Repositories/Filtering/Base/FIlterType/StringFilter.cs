﻿namespace WebApiPlayGround.Repositories.Filtering.Base.FilterType
{
    public class StringFilter
    {
        public string Equal { get; set; }
        public string NotEqual { get; set; }
        public string Contains { get; set; }
        public string NotContains { get; set; }
        public string StartsWith { get; set; }
        public string NotStartsWith { get; set; }
        public string EndsWith { get; set; }
        public string NotEndsWith { get; set; }

        public StringFilter ToLower()
        {
            if (Equal != null) Equal = Equal.ToLower();
            if (NotEqual != null) NotEqual = NotEqual.ToLower();
            if (Contains != null) Contains = Contains.ToLower();
            if (NotContains != null) NotContains = NotContains.ToLower();
            if (StartsWith != null) StartsWith = StartsWith.ToLower();
            if (NotStartsWith != null) NotStartsWith = NotStartsWith.ToLower();
            if (EndsWith != null) EndsWith = EndsWith.ToLower();
            if (NotEndsWith != null) NotEndsWith = NotEndsWith.ToLower();
            return this;
        }

        public StringFilter ToUpper()
        {
            if (Equal != null) Equal = Equal.ToUpper();
            if (NotEqual != null) NotEqual = NotEqual.ToUpper();
            if (Contains != null) Contains = Contains.ToUpper();
            if (NotContains != null) NotContains = NotContains.ToUpper();
            if (StartsWith != null) StartsWith = StartsWith.ToUpper();
            if (NotStartsWith != null) NotStartsWith = NotStartsWith.ToUpper();
            if (EndsWith != null) EndsWith = EndsWith.ToUpper();
            if (NotEndsWith != null) NotEndsWith = NotEndsWith.ToUpper();
            return this;
        }
    }
}