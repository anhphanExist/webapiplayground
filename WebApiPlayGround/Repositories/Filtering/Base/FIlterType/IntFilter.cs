﻿namespace WebApiPlayGround.Repositories.Filtering.Base.FilterType
{
    public class IntFilter
    {
        public int? Equal { get; set; }
        public int? NotEqual { get; set; }
        public int? Less { get; set; }
        public int? LessEqual { get; set; }
        public int? Greater { get; set; }
        public int? GreaterEqual { get; set; }
    }
}