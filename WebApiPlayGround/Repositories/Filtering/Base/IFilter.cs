﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiPlayGround.Repositories.Filtering.Base
{
    internal interface IFilter<TEntity, TDao, TFilter>
    {
        public IQueryable<TDao> DynamicFilter(IQueryable<TDao> query, TFilter filter);
        public IQueryable<TDao> DynamicOrder(IQueryable<TDao> query, TFilter filter);
    }
}