﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories
{
    public interface IWalletRepository : IRepositoryBase<Wallet, WalletFilter>
    {
        
    }
    
    public class WalletRepository : RepositoryBase<Wallet, WalletDao, WalletFilter>, IWalletRepository
    {
        public WalletRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            
        }

        public override IQueryable<WalletDao> DynamicFilter(IQueryable<WalletDao> query, WalletFilter filter)
        {
            if(filter == null)
                return query.Where(q => false);
            if (filter.UserId != null)
                query = query.Where(q => q.UserId, filter.UserId);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.Name != null)
                query = query.Where(q => q.Name, filter.Name);
            if (filter.Balance != null)
                query = query.Where(q => q.Balance, filter.Balance);
            
            return query;
        }

        public override IQueryable<WalletDao> DynamicOrder(IQueryable<WalletDao> query, WalletFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case WalletOrder.Name:
                            query = query.OrderBy(q => q.Name);
                            break;
                        case WalletOrder.Balance:
                            query = query.OrderBy(q => q.Balance);
                            break;
                        default:
                            query = query.OrderBy(q => q.Cx);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case WalletOrder.Name:
                            query = query.OrderByDescending(q => q.Name);
                            break;
                        case WalletOrder.Balance:
                            query = query.OrderByDescending(q => q.Balance);
                            break;
                        default:
                            query = query.OrderByDescending(e => e.Cx);
                            break;
                    }
                    break;
                default:
                    query = query.OrderBy(q => q.Cx);
                    break;
            }

            return query;
        }

        public override async Task<List<Wallet>> DynamicSelect(IQueryable<WalletDao> query, WalletFilter filter)
        {
            if (filter.Selects.Contains(WalletSelect.All))
            {
                return await MapListDaoToEntity(query);
            }

            return await query.Select(q => new Wallet
            {
                Id = filter.Selects.Contains(WalletSelect.Id) ? q.Id : Guid.Empty,
                Balance = filter.Selects.Contains(WalletSelect.Balance) ? q.Balance : decimal.Zero,
                Name = filter.Selects.Contains(WalletSelect.Name) ? q.Name : null,
                UserId = filter.Selects.Contains(WalletSelect.UserId) ? q.UserId : null
            }).ToListAsync();
        }

        protected override async Task<List<Wallet>> MapListDaoToEntity(IQueryable<WalletDao> query)
        {
            return await query.Select(q => new Wallet()
            {
                Id = q.Id,
                Name = q.Name,
                Balance = q.Balance,
                UserId = q.UserId
            }).ToListAsync();
        }

        protected override IQueryable<WalletDao> MapListEntityToDao(List<Wallet> entities)
        {
            throw new System.NotImplementedException();
        }

        protected override Wallet MapDaoToEntity(WalletDao modelDao)
        {
            return new Wallet()
            {
                Id = modelDao.Id,
                Name = modelDao.Name,
                Balance = modelDao.Balance,
                UserId = modelDao.UserId
            };
        }

        protected override WalletDao MapEntityToDao(Wallet entity)
        {
            return new WalletDao()
            {
                Id = entity.Id,
                Name = entity.Name,
                Balance = entity.Balance,
                UserId = entity.UserId
            };
        }

        public override IQueryable<WalletDao> Search(IQueryable<WalletDao> query, WalletFilter filter)
        {
            throw new NotImplementedException();
        }
    }
}