﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories
{
    public interface ICategoryRepository : IRepositoryBase<Category, CategoryFilter>
    {
        
    }
    
    public class CategoryRepository : RepositoryBase<Category, CategoryDao, CategoryFilter>, ICategoryRepository
    {
        public CategoryRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public override IQueryable<CategoryDao> DynamicFilter(IQueryable<CategoryDao> query, CategoryFilter filter)
        {
            if (filter == null)
                return query.Where(q => 1 == 0);
            if (filter.Id != null) 
                query = query.Where(q => q.Id, filter.Id);
            if (filter.Name != null)
                query = query.Where(q => q.Name, filter.Name);
            if (filter.Type != null)
                query = query.Where(q => q.Type == filter.Type);
  
            return query;
        }

        public override IQueryable<CategoryDao> DynamicOrder(IQueryable<CategoryDao> query, CategoryFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case CategoryOrder.Name:
                            query = query.OrderBy(g => g.Name);
                            break;
                        case CategoryOrder.Type:
                            query = query.OrderBy(g => g.Type);
                            break;
                        default:
                            query = query.OrderBy(e => e.Cx);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case CategoryOrder.Name:
                            query = query.OrderByDescending(g => g.Name);
                            break;
                        case CategoryOrder.Type:
                            query = query.OrderByDescending(g => g.Type);
                            break;
                        default:
                            query = query.OrderByDescending(e => e.Cx);
                            break;
                    }
                    break;
                default:
                    query = query.OrderBy(e => e.Cx);
                    break;
            }

            return query;
        }

        public override async Task<List<Category>> DynamicSelect(IQueryable<CategoryDao> query, CategoryFilter filter)
        {
            if (filter.Selects.Contains(CategorySelect.All))
            {
                return await MapListDaoToEntity(query);
            }

            return await query.Select(q => new Category()
            {
                Id = q.Id,
                Name = q.Name,
                Type = q.Type == false ? CategoryType.Expense : CategoryType.Income,
                ImageUrl = q.ImageUrl
            }).ToListAsync();
        }

        protected override async Task<List<Category>> MapListDaoToEntity(IQueryable<CategoryDao> query)
        {
            return await query.Select(q => new Category()
            {
                Id = q.Id,
                Name = q.Name,
                Type = q.Type == false ? CategoryType.Expense : CategoryType.Income,
                ImageUrl = q.ImageUrl
            }).ToListAsync();
        }

        protected override IQueryable<CategoryDao> MapListEntityToDao(List<Category> entities)
        {
            throw new NotImplementedException();
        }

        protected override Category MapDaoToEntity(CategoryDao modelDao)
        {
            return new Category()
            {
                Id = modelDao.Id,
                Name = modelDao.Name,
                Type = modelDao.Type == false ? CategoryType.Expense : CategoryType.Income,
                ImageUrl = modelDao.ImageUrl
            };
        }

        protected override CategoryDao MapEntityToDao(Category entity)
        {
            return new CategoryDao()
            {
                Id = entity.Id,
                Name = entity.Name,
                Type = entity.Type != CategoryType.Expense,
                ImageUrl = entity.ImageUrl
            };
        }

        public override IQueryable<CategoryDao> Search(IQueryable<CategoryDao> query, CategoryFilter filter)
        {
            throw new NotImplementedException();
        }
    }
}