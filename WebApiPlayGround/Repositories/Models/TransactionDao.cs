﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebApiPlayGround.Repositories.Models
{
    [Table("Transaction")]
    [Index(nameof(Cx), Name = "transaction_cx_idx", IsUnique = true)]
    public class TransactionDao : BaseModelDao
    {
        public long Cx { get; set; }
        
        [Column(TypeName = "numeric")]
        public decimal Amount { get; set; }
        
        [StringLength(200)]
        public string Note { get; set; }
        
        public DateTime Date { get; set; }
        
        public Guid WalletId { get; set; }
        public Guid CategoryId { get; set; }

        public virtual CategoryDao Category { get; set; }
        public virtual WalletDao Wallet { get; set; }
    }
}