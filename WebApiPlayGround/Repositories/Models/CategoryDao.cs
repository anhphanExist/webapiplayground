﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebApiPlayGround.Repositories.Models
{
    [Table("Category")]
    [Index(nameof(Cx), Name = "category_cx_idx", IsUnique = true)]
    public class CategoryDao : BaseModelDao
    {
        public long Cx { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        
        public bool Type { get; set; }
        
        public string ImageUrl { get; set; }

        public virtual ICollection<TransactionDao> Transactions { get; set; }
    }
}