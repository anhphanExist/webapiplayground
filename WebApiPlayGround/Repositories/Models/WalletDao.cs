﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebApiPlayGround.Repositories.Models
{
    [Table("Wallet")]
    [Index(nameof(Cx), Name= "wallet_cx_idx", IsUnique = true)]
    public class WalletDao : BaseModelDao
    {
        public long Cx { get; set; }
        
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        
        [Column(TypeName = "numeric")]
        public decimal Balance { get; set; }
        
        public string UserId { get; set; }
        
        public virtual UserDao User { get; set; }
        public virtual ICollection<TransactionDao> Transactions { get; set; }
    }
}