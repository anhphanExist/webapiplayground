﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebApiPlayGround.Repositories.Models
{
    [Table("User")]
    [Index(nameof(Cx), Name = "user_cx_idx", IsUnique = true)]
    public class UserDao : IdentityUser, IBaseModel
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        
        public long Cx { get; set; }
        
        public bool IsDeleted { get; set; }
        public DateTime DeletedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        
        public ICollection<WalletDao> Wallets { get; set; }
    }
}