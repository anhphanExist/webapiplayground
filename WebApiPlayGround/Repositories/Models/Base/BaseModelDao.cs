﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApiPlayGround.Repositories.Models.Base
{
    public interface IBaseModel
    {
        bool IsDeleted { get; set; }
        DateTime DeletedAt { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
    }

    public class BaseModelDao : IBaseModel
    {
        [Key]
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}