﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebApiPlayGround.Repositories.Models.Base
{
    public class InitialUserConfiguration : IEntityTypeConfiguration<UserDao>
    {
        public void Configure(EntityTypeBuilder<UserDao> builder)
        {
            builder.HasData
            (
                new UserDao()
                {
                    UserName = "anhphan",
                    Email = "anhphan.csf@gmail.com",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    FirstName = "Anh",
                    LastName = "Phan",
                    EmailConfirmed = true,
                    PasswordHash = "123456",
                    PhoneNumber = "096 564 2598"
                }
            );
        }
    }
}