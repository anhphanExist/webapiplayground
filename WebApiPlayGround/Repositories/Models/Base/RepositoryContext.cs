﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApiPlayGround.Repositories.Models.Base
{
    public class RepositoryContext : IdentityDbContext<UserDao>
    {
        public virtual DbSet<CategoryDao> Categories { get; set; }
        public virtual DbSet<TransactionDao> Transactions { get; set; }
        public virtual DbSet<WalletDao> Wallets { get; set; }
        
        public RepositoryContext(DbContextOptions<RepositoryContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new InitialUserConfiguration());
            
            builder.Entity<CategoryDao>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);
                
                entity.HasIndex(e => e.Cx)
                    .IsClustered();

                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.Property(e => e.Cx).ValueGeneratedOnAdd().Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore);
            });

            builder.Entity<TransactionDao>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);
                
                entity.HasIndex(e => e.Cx)
                    .IsClustered();
                
                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.Property(e => e.Cx).ValueGeneratedOnAdd().Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("transaction_fk_category");

                entity.HasOne(d => d.Wallet)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.WalletId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("transaction_fk_wallet");
            });

            builder.Entity<UserDao>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);
                
                entity.HasIndex(e => e.Cx)
                    .IsClustered();
                
                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.Property(e => e.Cx).ValueGeneratedOnAdd().Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore);
            });

            builder.Entity<WalletDao>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);
                
                entity.HasIndex(e => e.Cx)
                    .IsClustered();
                
                entity.Property(e => e.Id).ValueGeneratedNever();
                entity.Property(e => e.Cx).ValueGeneratedOnAdd().Metadata.SetAfterSaveBehavior(PropertySaveBehavior.Ignore);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Wallets)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("wallet_fk");
            });
            
            
            //other manual configurations left out

            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                //other automated configurations left out
                if (typeof(BaseModelDao).IsAssignableFrom(entityType.ClrType))
                {
                    entityType.AddSoftDeleteQueryFilter();
                }
            }
        }
    }
    
    public static class SoftDeleteQueryExtension
    {
        public static void AddSoftDeleteQueryFilter(this IMutableEntityType entityData)
        {
            var methodToCall = typeof(SoftDeleteQueryExtension)
                .GetMethod(nameof(GetSoftDeleteFilter), BindingFlags.NonPublic | BindingFlags.Static)
                ?.MakeGenericMethod(entityData.ClrType);
            var filter = methodToCall?.Invoke(null, Array.Empty<object>());
            if (filter != null)
            {
                entityData.SetQueryFilter((LambdaExpression) filter);
            }
        }

        private static LambdaExpression GetSoftDeleteFilter<TEntity>() where TEntity : BaseModelDao
        {
            Expression<Func<TEntity, bool>> filter = x => !x.IsDeleted;
            return filter;
        }
    }
}