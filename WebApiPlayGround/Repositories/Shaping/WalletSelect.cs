﻿using System;

namespace WebApiPlayGround.Repositories.Shaping
{
    [Flags]
    public enum WalletSelect : long
    {
        All = DataShaper.All,
        Id = DataShaper._1,
        Name = DataShaper._2,
        Balance = DataShaper._3,
        UserId = DataShaper._4
    }
}