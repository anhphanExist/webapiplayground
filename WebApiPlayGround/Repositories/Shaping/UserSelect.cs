﻿using System;

namespace WebApiPlayGround.Repositories.Shaping
{
    [Flags]
    public enum UserSelect : long
    {
        All = DataShaper.All,
        Id = DataShaper._1,
        UserName = DataShaper._2,
        FirstName = DataShaper._3,
        LastName = DataShaper._4,
        Email = DataShaper._5
    }
}