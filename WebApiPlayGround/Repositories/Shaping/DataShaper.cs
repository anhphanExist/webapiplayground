﻿using System;

namespace WebApiPlayGround.Repositories.Shaping
{
    public static class DataShaper
    {
        public static bool Contains(this Enum source, Enum destination)
        {
            long sourceValue = Convert.ToInt64(source);
            long destValue = Convert.ToInt64(destination);
            return (sourceValue & destValue) == destValue;
        }

        public const long All = 9223372036854775807;
        public const long _1 = 1;
        public const long _2 = 2;
        public const long _3 = 4;
        public const long _4 = 8;
        public const long _5 = 16;
        public const long _6 = 32;
        public const long _7 = 64;
        public const long _8 = 128;
        public const long _9 = 256;
        public const long _10 = 512;
        public const long _11 = 1024;
        public const long _12 = 2048;
        public const long _13 = 4096;
        public const long _14 = 8192;
        public const long _15 = 16384;
        public const long _16 = 32768;
        public const long _17 = 65536;
        public const long _18 = 131072;
        public const long _19 = 262144;
        public const long _20 = 524288;
        public const long _21 = 1048576;
        public const long _22 = 2097152;
        public const long _23 = 4194304;
        public const long _24 = 8388608;
        public const long _25 = 16777216;
        public const long _26 = 33554432;
        public const long _27 = 67108864;
        public const long _28 = 134217728;
        public const long _29 = 268435456;
        public const long _30 = 536870912;
        public const long _31 = 1073741824;
        public const long _32 = 2147483648;
        public const long _33 = 4294967296;
        public const long _34 = 8589934592;
        public const long _35 = 17179869184;
        public const long _36 = 34359738368;
        public const long _37 = 68719476736;
        public const long _38 = 137438953472;
        public const long _39 = 274877906944;
        public const long _40 = 549755813888;
        public const long _41 = 1099511627776;
        public const long _42 = 2199023255552;
        public const long _43 = 4398046511104;
        public const long _44 = 8796093022208;
        public const long _45 = 17592186044416;
        public const long _46 = 35184372088832;
        public const long _47 = 70368744177664;
        public const long _48 = 140737488355328;
        public const long _49 = 281474976710656;
        public const long _50 = 562949953421312;
        public const long _51 = 1125899906842620;
        public const long _52 = 2251799813685250;
        public const long _53 = 4503599627370500;
        public const long _54 = 9007199254740990;
        public const long _55 = 18014398509482000;
        public const long _56 = 36028797018964000;
        public const long _57 = 72057594037927900;
        public const long _58 = 144115188075856000;
        public const long _59 = 288230376151712000;
        public const long _60 = 576460752303423000;
        public const long _61 = 1152921504606850000;
        public const long _62 = 2305843009213690000;
    }
}