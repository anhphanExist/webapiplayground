﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiPlayGround.Repositories.Shaping
{
    public interface IShape<TEntity, TDao, TFilter>
    {
        public Task<List<TEntity>> DynamicSelect(IQueryable<TDao> query, TFilter filter);
    }
}