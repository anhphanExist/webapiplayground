﻿using System;

namespace WebApiPlayGround.Repositories.Shaping
{
    [Flags]
    public enum TransactionSelect : long
    {
        All = DataShaper.All,
        Id = DataShaper._1,
        WalletId = DataShaper._2,
        WalletName = DataShaper._3,
        CategoryId = DataShaper._4,
        CategoryName = DataShaper._5,
        Amount = DataShaper._6,
        Note = DataShaper._7,
        Date = DataShaper._8,
        UserId = DataShaper._9
    }
}