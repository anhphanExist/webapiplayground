﻿using System;

namespace WebApiPlayGround.Repositories.Shaping
{
    [Flags]
    public enum CategorySelect : long
    {
        All = DataShaper.All,
        Id = DataShaper._1,
        Name = DataShaper._2,
        Type = DataShaper._3,
        ImageUrl = DataShaper._4
    }
}