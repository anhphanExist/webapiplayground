﻿using System.Threading.Tasks;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebApiPlayGround.Repositories.Configuration
{
    public interface IUnitOfWork
    {
        Task Begin();
        Task Commit();
        Task Rollback();
        Task Save();
        ICategoryRepository CategoryRepository { get; }
        ITransactionRepository TransactionRepository { get; }
        IUserRepository UserRepository { get; }
        IWalletRepository WalletRepository { get; }
    }
    
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RepositoryContext _repositoryContext;
        
        public ICategoryRepository CategoryRepository { get; }
        public ITransactionRepository TransactionRepository { get; }
        public IUserRepository UserRepository { get; }
        public IWalletRepository WalletRepository { get; }

        public UnitOfWork(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
            CategoryRepository = new CategoryRepository(repositoryContext);
            TransactionRepository = new TransactionRepository(repositoryContext);
            UserRepository = new UserRepository(repositoryContext);
            WalletRepository = new WalletRepository(repositoryContext);
        }

        
        
        
        public async Task Begin()
        {
            await _repositoryContext.Database.BeginTransactionAsync();
        }

        public async Task Commit()
        {
            await _repositoryContext.Database.CommitTransactionAsync();
        }

        public async Task Rollback()
        {
            await _repositoryContext.Database.RollbackTransactionAsync();
        }

        public async Task Save() => await _repositoryContext.SaveChangesAsync();
    }
}