﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Services.BusinessEntities.Base;
using WebApiPlayGround.Repositories.Models.Base;
using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Paging;
using WebApiPlayGround.Repositories.Searching;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories.Configuration
{
    public interface IRepositoryBase<TEntity, TFilter>
    {
        Task<int> Count(TFilter filter);
        Task<List<TEntity>> List(TFilter filter);
        Task<TEntity> Get(Guid Id);
        Task<TEntity> Get(TFilter filter);
        Task Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(Guid Id);
    }
    
    public abstract class RepositoryBase<TEntity, TDao, TFilter> : IRepositoryBase<TEntity, TFilter>, 
        ISearch<TDao, TFilter>,
        IFilter<TEntity, TDao, TFilter>,
        IShape<TEntity, TDao, TFilter>
        where TEntity : BaseBusinessEntity
        where TDao : BaseModelDao
        where TFilter : BaseFilter
    {
        protected readonly RepositoryContext RepositoryContext;

        protected RepositoryBase(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public async Task<int> Count(TFilter filter)
        {
            IQueryable<TDao> query = RepositoryContext.Set<TDao>().AsNoTracking();
            query = DynamicFilter(query, filter);
            return await query.CountAsync();
        }

        public async Task<List<TEntity>> List(TFilter filter)
        {
            IQueryable<TDao> query = RepositoryContext.Set<TDao>().AsNoTracking();
            // query = Search(query, filter);
            query = DynamicFilter(query, filter);
            query = DynamicOrder(query, filter);
            query = query.Paginate(filter.CurrentPage, filter.PageSize);
            return await DynamicSelect(query, filter);
        }

        public async Task<TEntity> Get(Guid Id)
        {
            TDao modelDao = await RepositoryContext.Set<TDao>().FirstOrDefaultAsync(dao => dao.Id.Equals(Id));
            return MapDaoToEntity(modelDao);
        }

        public async Task<TEntity> Get(TFilter filter)
        {
            IQueryable<TDao> query = RepositoryContext.Set<TDao>().AsNoTracking();
            TDao modelDao = DynamicFilter(query, filter).FirstOrDefault();
            return MapDaoToEntity(modelDao);
        }

        public async Task Create(TEntity entity) => RepositoryContext.Set<TDao>().Add(MapEntityToDao(entity));
        public async Task Update(TEntity entity) => RepositoryContext.Set<TDao>().Update(MapEntityToDao(entity));

        public async Task Delete(Guid Id)
        {
            TDao modelDao = await RepositoryContext.Set<TDao>().FirstOrDefaultAsync(dao => dao.Id.Equals(Id));
            RepositoryContext.Set<TDao>().Remove(modelDao);
        }

        public abstract IQueryable<TDao> DynamicFilter(IQueryable<TDao> query, TFilter filter);
        public abstract IQueryable<TDao> DynamicOrder(IQueryable<TDao> query, TFilter filter);
        public abstract Task<List<TEntity>> DynamicSelect(IQueryable<TDao> query, TFilter filter);
        protected abstract Task<List<TEntity>> MapListDaoToEntity(IQueryable<TDao> query);
        protected abstract IQueryable<TDao> MapListEntityToDao(List<TEntity> entities);
        protected abstract TEntity MapDaoToEntity(TDao modelDao);
        protected abstract TDao MapEntityToDao(TEntity entity);

        public abstract IQueryable<TDao> Search(IQueryable<TDao> query, TFilter filter);
    }
}