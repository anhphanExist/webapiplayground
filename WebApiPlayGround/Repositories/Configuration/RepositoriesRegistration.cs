﻿using Microsoft.Extensions.DependencyInjection;

namespace WebApiPlayGround.Repositories.Configuration
{
    public static class RepositoriesRegistration
    {
        public static void ConfigureRepositoriesLayer(this IServiceCollection services, bool registerOdbc = false)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}