﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;
using WebApiPlayGround.Repositories.Filtering;
using WebApiPlayGround.Repositories.Filtering.Base;
using WebApiPlayGround.Repositories.Shaping;

namespace WebApiPlayGround.Repositories
{
    public interface ITransactionRepository : IRepositoryBase<Transaction, TransactionFilter>
    {
        
    }
    
    public class TransactionRepository : RepositoryBase<Transaction, TransactionDao, TransactionFilter>, ITransactionRepository
    {
        public TransactionRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
            
        }

        public override IQueryable<TransactionDao> DynamicFilter(IQueryable<TransactionDao> query, TransactionFilter filter)
        {
            if (filter == null)
                return query.Where(q => false);
            if (filter.UserId != null)
                query = query.Where(q => q.Wallet.UserId, filter.UserId);
            if (filter.WalletId != null)
                query = query.Where(q => q.WalletId, filter.WalletId);
            if (filter.WalletName != null)
                query = query.Where(q => q.Wallet.Name, filter.WalletName);
            if (filter.CategoryId != null)
                query = query.Where(q => q.CategoryId, filter.CategoryId);
            if (filter.CategoryName != null)
                query = query.Where(q => q.Category.Name, filter.CategoryName);
            if (filter.Id != null)
                query = query.Where(q => q.Id, filter.Id);
            if (filter.Date != null)
                query = query.Where(q => 
                    q.Date.Day == filter.Date.Equal.Value.Day && 
                    q.Date.Month == filter.Date.Equal.Value.Month && 
                    q.Date.Year == filter.Date.Equal.Value.Year
                );
            return query;
        }

        public override IQueryable<TransactionDao> DynamicOrder(IQueryable<TransactionDao> query, TransactionFilter filter)
        {
            switch (filter.OrderType)
            {
                case OrderType.ASC:
                    switch (filter.OrderBy)
                    {
                        case TransactionOrder.WalletId:
                            query = query.OrderBy(g => g.WalletId);
                            break;
                        case TransactionOrder.CategoryId:
                            query = query.OrderBy(g => g.CategoryId);
                            break;
                        default:
                            query = query.OrderBy(g => g.Cx);
                            break;
                    }
                    break;
                case OrderType.DESC:
                    switch (filter.OrderBy)
                    {
                        case TransactionOrder.WalletId:
                            query = query.OrderByDescending(g => g.WalletId);
                            break;
                        case TransactionOrder.CategoryId:
                            query = query.OrderByDescending(g => g.CategoryId);
                            break;
                        default:
                            query = query.OrderByDescending(g => g.Cx);
                            break;
                    }
                    break;
                default:
                    query = query.OrderBy(e => e.Cx);
                    break;
            }

            return query;
        }

        public override async Task<List<Transaction>> DynamicSelect(IQueryable<TransactionDao> query,
            TransactionFilter filter)
        {
            if (filter.Selects.Contains(TransactionSelect.All))
            {
                return await MapListDaoToEntity(query);
            }
            return await query.Select(q => new Transaction()
            {
                Id = filter.Selects.Contains(TransactionSelect.Id) ? q.Id : Guid.Empty,
                WalletId = filter.Selects.Contains(TransactionSelect.WalletId) ? q.WalletId : Guid.Empty,
                WalletName = filter.Selects.Contains(TransactionSelect.WalletName) ? q.Wallet.Name : null,
                CategoryId = filter.Selects.Contains(TransactionSelect.CategoryId) ? q.CategoryId : Guid.Empty,
                CategoryName = filter.Selects.Contains(TransactionSelect.CategoryName) ? q.Category.Name : null,
                Amount = filter.Selects.Contains(TransactionSelect.Amount) ? q.Amount : decimal.Zero,
                Date = filter.Selects.Contains(TransactionSelect.Date) ? q.Date : DateTime.MinValue,
                Note = filter.Selects.Contains(TransactionSelect.Note) ? q.Note : null,
                UserId = filter.Selects.Contains(TransactionSelect.UserId) ? q.Wallet.UserId : null
            }).ToListAsync();
        }

        protected override async Task<List<Transaction>> MapListDaoToEntity(IQueryable<TransactionDao> query)
        {
            return await query.Select(q => new Transaction()
            {
                Id = q.Id,
                WalletId = q.WalletId,
                WalletName = q.Wallet.Name,
                CategoryId = q.CategoryId,
                CategoryName = q.Category.Name,
                Amount = q.Category.Type? q.Amount : 0 - q.Amount,
                Note = q.Note,
                Date = q.Date,
                UserId = q.Wallet.UserId
            }).ToListAsync();
        }

        protected override IQueryable<TransactionDao> MapListEntityToDao(List<Transaction> entities)
        {
            throw new System.NotImplementedException();
        }

        protected override Transaction MapDaoToEntity(TransactionDao modelDao)
        {
            return new Transaction()
            {
                Id = modelDao.Id,
                WalletId = modelDao.WalletId,
                WalletName = modelDao.Wallet.Name,
                CategoryId = modelDao.CategoryId,
                CategoryName = modelDao.Category.Name,
                Amount = modelDao.Category.Type? modelDao.Amount : 0 - modelDao.Amount,
                Note = modelDao.Note,
                Date = modelDao.Date,
                UserId = modelDao.Wallet.UserId
            };
        }

        protected override TransactionDao MapEntityToDao(Transaction entity)
        {
            return new TransactionDao()
            {
                Id = entity.Id,
                WalletId = entity.WalletId,
                CategoryId = entity.CategoryId,
                Amount = entity.Amount,
                Note = entity.Note,
                Date = entity.Date
            };
        }

        public override IQueryable<TransactionDao> Search(IQueryable<TransactionDao> query, TransactionFilter filter)
        {
            throw new NotImplementedException();
        }
    }
}