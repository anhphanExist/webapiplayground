﻿using System.Linq;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;

namespace WebApiPlayGround.Repositories.Searching
{
    public interface ISearch<TDao, TFilter>
    {
        IQueryable<TDao> Search(IQueryable<TDao> query, TFilter filter);
    }
}