﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApiPlayGround.Repositories.Configuration;
using WebApiPlayGround.Repositories.Models;
using WebApiPlayGround.Repositories.Models.Base;
using WebApiPlayGround.Services.BusinessEntities;
using WebApiPlayGround.Repositories.Filtering;

namespace WebApiPlayGround.Repositories
{
    public interface IUserRepository : IRepositoryBase<User, UserFilter>
    {
        
    }
    
    public class UserRepository : IUserRepository
    {
        private readonly RepositoryContext _repositoryContext;
        public UserRepository(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }


        public async Task<int> Count(UserFilter filter)
        {
            throw new NotImplementedException();
        }

        public async Task<List<User>> List(UserFilter filter)
        {
            throw new NotImplementedException();
        }

        public async Task<User> Get(Guid Id)
        {
            throw new NotImplementedException();
        }

        public async Task<User> Get(UserFilter filter)
        {
            throw new NotImplementedException();
        }

        public async Task Create(User entity)
        {
            throw new NotImplementedException();
        }

        public async Task Update(User entity)
        {
            throw new NotImplementedException();
        }

        public async Task Delete(Guid Id)
        {
            throw new NotImplementedException();
        }
    }
}