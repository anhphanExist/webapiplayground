﻿using System.Collections.Generic;

namespace WebApiPlayGround.Repositories.Paging
{
    public class PagedModel<TEntity>
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
        public IList<TEntity> Items { get; set; }

        public PagedModel()
        {
            Items = new List<TEntity>();
        }
    }
}