﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApiPlayGround.Repositories.Models.Base;

namespace WebApiPlayGround.Repositories.Paging
{
    public static class DataPagerExtension
    {
        public static PagedModel<TEntity> ToPageModel<TEntity>(this List<TEntity> items, int page, int pageSize)
        {
            return new PagedModel<TEntity>
            {
                Items = items,
                TotalItems = items.Count,
                CurrentPage = page,
                PageSize = pageSize,
                TotalPages = (int) Math.Ceiling(items.Count / (double) pageSize)
            };
        }

        public static IQueryable<TDao> Paginate<TDao>(this IQueryable<TDao> query, int page, int pageSize)
            where TDao : BaseModelDao
        {
            var startRow = (page - 1) * pageSize;
            return query
                .Skip(startRow)
                .Take(pageSize);
        }
    }
}